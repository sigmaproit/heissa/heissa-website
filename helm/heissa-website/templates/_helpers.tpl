{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "heissa-website.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "heissa-website.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- tpl (.Values.fullnameOverride) $ | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "heissa-website.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "heissa-website.labels" -}}
helm.sh/chart: {{ include "heissa-website.chart" . }}
{{ include "heissa-website.selectorLabels" . }}
{{- if (include "heissa-website.appVersion" .) }}
app.kubernetes.io/version: {{ include "heissa-website.appVersion" . | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "heissa-website.selectorLabels" -}}
app.kubernetes.io/name: {{ include "heissa-website.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}


{{/*
Release name
*/}}
{{- define "heissa-website.releaseName" -}}
{{- .Release.Name -}}
{{- end -}}

{{/*
appVersion
*/}}
{{- define "heissa-website.appVersion" -}}
{{- if .Values.appVersionOverride }}
{{- .Values.appVersionOverride -}}
{{- else if .Chart.AppVersion }}
{{- .Chart.AppVersion -}}
{{- end -}}
{{- end -}}
