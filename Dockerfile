FROM jekyll/jekyll:3.8 AS builder

WORKDIR /jekyll

COPY Gemfile* /jekyll/
RUN bundle install

COPY . /jekyll
RUN jekyll build

#ENTRYPOINT ["jekyll"]
#CMD ["serve", "--host", "0.0.0.0", "--port", "80"]

FROM nginx:alpine

COPY --from=builder /jekyll/_site /usr/share/nginx/html
COPY --from=builder /jekyll/nginx.conf  /etc/nginx/conf.d/default.conf
