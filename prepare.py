import json
import os

basic = {
    "both_single_value": "the value of no langauge",
    "both_single_object": {"a": "a", "b": "b"},
    "both_array_values": [1, 2, 3, 4],
    "both_array_objects": [{1: 1}, {2: 2}, {3: 3}],
    
    "english_single_value__en": "the value of no langauge",
    "english_single_object__en": {"a": "a", "b": "b"},
    "english_array_values__en": [1, 2, 3, 4],
    "english_array_objects__en": [{1: 1}, {2: 2}, {3: 3}],

    "arabic_single_value__ar": "the value of no langauge",
    "arabic_single_object__ar": {"a": "a", "b": "b"},
    "arabic_array_values__ar": [1, 2, 3, 4],
    "arabic_array_objects__ar": [{1: 1}, {2: 2}, {3: 3}],
}
root = {
    **basic,  "tree": {"l11": {**basic}, "l12": {**basic}, "arr": [{**basic}, {**basic}, "value1", "value2"] }
}

def split_dynamic(root, langs = ['en', 'ar']):
    res = {l: root for l in langs}
    if (isinstance(root, dict)):
        res = { l: {} for l in langs}
        for k in root.keys():
            kStr = str(k)
            langSpecific = False
            for lang in langs:
                if(kStr.endswith("__" + lang)):
                    res[lang].update({kStr[0:-len(lang) - 2]: root[k]})
                    langSpecific = True
                    break
            if not langSpecific:
                _res = split_dynamic(root[k])
                for lang, value in _res.items():
                    if (not value): continue # no values in this language !
                    res[lang].update({k: value})
    elif (isinstance(root, list)):
        res = { l:[] for l in langs}
        for item in root:
            _res = split_dynamic(item)
            for lang, value in _res.items():
                if (not value): continue # no values in this language !
                res[lang].append(value)
    return res

def translation(langs=['en', 'ar']):
    dirBase = "_translate_src"
    outDirBase = "_data"
    filenames = os.listdir(dirBase)
    res = { l: {} for l in langs}
    for filename in filenames:
        with open(os.path.join(dirBase, filename), 'r') as currentFile:
            jsonstr = currentFile.read()
            root = json.loads(jsonstr)
        currentRes = split_dynamic(root)
        for lang, value in currentRes.items():
            res[lang].update({filename.replace(".json", ""): value})
    for lang, value in res.items():
        with open(os.path.join(outDirBase, lang + ".json"), 'w') as outFile:
            outFile.write(json.dumps(value))



def test():
    for lang, value in split_dynamic(root).items():
        with open(f"{lang}.json", "w") as outFile:
            outFile.write(json.dumps(value))

def main(langs=['en', 'ar']):
    translation(langs=langs)
if __name__ == "__main__":
    main()