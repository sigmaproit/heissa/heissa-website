---
layout: blog_post
date: 2020-03-10T00:00:00.000+02:00
mediaType: " image"
title: Port Said Bird Festival - 23rd of March
excerpt: Port Said Bird Festival, People from Port Said can celebrate their beautiful
  photographs with other bird lovers
mainImage: "/static/images/12.jpg"
images:
- "/static/images/11.jpg"
- "/static/images/hashim.png"
- "/static/images/nader.png"
quote:
  text: Port Said Bird Festival, People from Port Said can celebrate their beautiful
    photographs with other bird lovers
  city: 23rd of March
linkTitle: ''
vurl: https://www.youtube.com/watch?v=5eFDrW5Bhl4
relatedPosts:
- _posts/2022-11-07-who-are-we.md
- _posts/our-story.md
- _posts/aza-ola.md

---
The only regret is regretting not starting, through a 14 years long journey of small steps and many official and friendly meetings, the people of Port Said now can finally hold a pair binoculars and observe wintering birds, not only that, people from Port Said can celebrate their beautiful photographs with other bird lovers from around Egypt in a government organized event (Port Said Bird Festival) that will include lots of activities with the main focus of raising the eco-awareness.