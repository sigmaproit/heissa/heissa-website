/// <reference path="../../../node_modules/@types/jquery/index.d.ts" />

var HeissaRoomsChooser = (function($){
    'use strict';

    return {
        /**
         * Displays slide by specified index.
         * @param {Object} roomData
         * @returns {jQuery}
         */
        init: function(payload) {
            var rooms = payload
            // console.log("rooms in heissa-rooms-chooser is: ", JSON.parse(JSON.stringify(rooms)) )
            rooms = HeissaHelpers.dicToArr(rooms)
            rooms.sort((a, b) => b.filled_beds - a.filled_beds)
            var $elements = $('')
            rooms.forEach(roomPayload => {
                roomPayload.onValueChanged = () => payload.onChange(roomPayload.name)
                let $currentElement = HeissaRoomChooser.init(roomPayload)
                $elements = $elements.add($currentElement)
            })
            return $elements
        }
    };
})(window.jQuery);
