/// <reference path="../../../node_modules/@types/jquery/index.d.ts" />


const template = `
<!--================ Entities (Style 15) ================-->
<div class="milenia-entities milenia-entities--style-15 milenia-entities--with-tabbed-grid">
    <div id="milenia-grid-tabbed-1"
        class="milenia-grid milenia-grid--cols-1 milenia-grid--tabbed milenia-grid--tabbed-loading">
        {{#each rooms}}
        <div class="milenia-grid-item">
            <!--================ Entity ================-->
            <article class="milenia-entity">
                <div class="milenia-entity-media">
                    <div class="owl-carousel owl-carousel--vadaptive milenia-simple-slideshow">
                        {{#each this.images}}
                        <div data-bg-image-src='{{this}}' class="milenia-entity-slide"></div>
                        {{/each}}
                    </div>
                </div>
                <div class="milenia-entity-content milenia-aligner">
                    <div class="milenia-aligner-outer">
                        <div class="milenia-aligner-inner">
                            <header class="milenia-entity-header">
                                <span>
                                    <span class="milenia-entity-title span-header milenia-color--unchangeable">{{this.display_name}}</span>
                                    {{#each this.benefits}}
                                    <i title="{{this.display_name}}" class="{{this.icon}} heissa-feature-icon"></i>
                                    {{/each}}
                                    <i title="hold up to {{this.beds_count}} persons" data-capacity="{{this.beds_count}}"
                                    class="milenia-font-icon-double-bed heissa-feature-icon heissa-feature-icon--badge"></i>
                                </span>
                                {{#if this.show_starting_price}}
                                <div class="milenia-entity-meta">
                                    <div>from <strong class='milenia-entity-price'>{{this.starting_price.value}} {{this.starting_price.currency}}</strong>/night</div>
                                </div>
                                {{/if}}
                            </header>
                            <div class="milenia-entity-body">
                                <p>{{this.desc}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        {{/each}}

    </div>
</div>
<div class="milenia-tabbed-carousel-thumbs">
    <div data-tabbed-sync="milenia-grid-tabbed-1"
        class="milenia-grid milenia-grid--cols-4 owl-carousel--nav-onhover owl-carousel owl-carousel--nav-edges owl-carousel--nav-small">
        {{#each rooms}}
        <div class="milenia-grid-item">
            <figure class="milenia-tabbed-carousel-thumb">
                <div data-bg-image-src="{{this.images.[0]}}" class="milenia-tabbed-carousel-thumb-image"></div>
                <figcaption class="milenia-tabbed-carousel-thumb-caption milenia-text-color--darkest">
                    {{this.display_name}}</figcaption>
            </figure>
        </div>
        {{/each}}
    </div>
</div>
`
var HeissaRoomsClassifications = (function($){
    'use strict';
    return {
        init: function(roomsData) {
            var $component = $(Handlebars.compile(template)(roomsData))
            return $component
        }
    };
})(window.jQuery);
