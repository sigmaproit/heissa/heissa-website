/// <reference path="../../../node_modules/@types/jquery/index.d.ts" />

var HeissaProgressbar = (function ($) {
    'use strict';

    var constructStrFromMinMax = (min, max) => `${min} / ${max} persons are included`
    var templateHtml = () => `
    <div>
        <div class="heissa-progress">
          <div class="heissa-bar"></div>
        </div>
        <p> ${constructStrFromMinMax(0, 0)} </p>
    </div>
    `

    /**
     * @constructor
     */
    function Progressbar(ele, config){
      var self = this
      this.$ele = ele
      this.config = config
      ele.append(templateHtml())
      console.log('in function Progressbar constructor, this is: ', this, ' and the element is ', ele)
    }

    Progressbar.prototype.update = function (progress) {
      const max = this.config.max
      const textEle = this.$ele.find('p')
      const heissaBarEle = this.$ele.find('.heissa-bar')
      if (progress === max) {// the user chooses the rooms correctly => make it success
        console.log('[Progressbar.update]: success as progress === max')
        heissaBarEle.addClass('heissa-bar--success').removeClass('heissa-bar--warn heissa-bar--error')
      }
      else if (progress > max) { // should warn the user
        console.log('[Progressbar.update] warning as progress > max')
        heissaBarEle.addClass('heissa-bar--warn').removeClass('heissa-bar--success heissa-bar--error')
      }
      else { // error, user should get a bed for every person
        console.log('[Progressbar.update] error as progress < max')
        heissaBarEle.addClass('heissa-bar--error').removeClass('heissa-bar--success heissa-bar--warn')
      }
      textEle.html(constructStrFromMinMax(progress, max))
      heissaBarEle.width(`${Math.min((progress/max) * 100, 100)}%`)
    }

    return {
        /**
         * Initializes existing progress-bars.
         *
         * @param {jQuery} $collection
         * @returns {jQuery}
         */
        init: function($collection, config) {
          return $collection.each(function(index, element) {
              var $element = $(element);
              if ($element.data('Progressbar')) { // if this item is initilized with the progressbar before don't init again
              $element.data('Progressbar').config = config
              return
             }
              var pb = new Progressbar($element, config)
              $element.data('Progressbar', pb);
              pb.update(0)
          });
      }
    };
})(window.jQuery);