var HeissaRoomChooser = (function ($) {
  'use strict';
  var availableRooms = HeissaHelpers.configToVar("_data/availableRooms.json")

  const template = `
  <div id="{{typeId}}" type="{{type}}" class="milenia-grid-item">
    <article class="milenia-entity">
      <div class="milenia-entity-media">
        <div class="owl-carousel owl-carousel--vadaptive milenia-simple-slideshow">
          {{#each images}}
          <div data-bg-image-src="{{this}}" class="milenia-entity-slide"></div>
          {{/each}}
        </div>
      </div>
      <div class="milenia-entity-content milenia-aligner">
        <div class="milenia-aligner-outer">
          <div class="milenia-aligner-inner">
            <header class="milenia-entity-header">
              <div class="heissa-rooms-available">{{available_rooms}} rooms available</div>
              {{#if show_starting_price_booking}}
              <div class="milenia-entity-meta">
                <div><strong id='{{typeId}}-start-price' class='milenia-entity-price price-value'>from {{starting_price}}
                    {{currency}}</strong>/night</div>
              </div>
              {{/if}}
              <span>
                <span class="milenia-entity-title">{{display_name}}</span>

                {{#each benefits}}
                {{#if this.icon}}
                <i title="{{this.display_name}}" class="{{this.icon}} heissa-feature-icon"></i>
                {{/if}}
                {{/each}}
                <i title="hold up to {{no_of_beds}} persons" data-capacity="{{no_of_beds}}"
                  class="milenia-font-icon-double-bed heissa-feature-icon heissa-feature-icon--badge"></i>
              </span>
            </header>
            <div class="milenia-entity-body">
              <p>{{description}}</p>
            </div>
            <footer class="milenia-entity-footer">
              <form class="milenia-booking-form milenia-booking-form--style-1">
                <div class="form-group row heissa-room-card-row">
                  <div class="col" style="border: thin solid black; text-align: center; margin-right: 7px;">
                    <div class="form-control" style="display: inline-block">
                      <div>
                        <label style="font-size: 1.1rem">{{roomsCounterLabel}}</label>
                        <span style="vertical-align: middle" data-tooltip="{{roomsCounterTooltip}}"
                          aria-describedby="monkeysan-tooltip-0"><i class="fas fa-question-circle heissa-icon-tooltip"></i></span>
                      </div>
                      <div class="milenia-field-counter" style="max-height:2rem; min-height: 2rem; padding-right: 1.5rem">
                        <p class="milenia-field-counter-value" style="font-size: 2rem; padding: 0px">{{filled_rooms}}</p>
            
                        <input id='{{typeId}}-rooms' type="hidden" name="booking-form-adults" value="{{filled_rooms}}"
                          class="milenia-field-counter-target">
                        <button type="button"
                          class="milenia-field-counter-control milenia-field-counter-control--decrease room-control-decrease"
                          style="font-size: 0.8rem" data-max={{available_rooms}}>
                        </button>
                        <button type="button"
                          class="milenia-field-counter-control milenia-field-counter-control--increase room-control-increase"
                          style="font-size: 0.8rem" data-max={{available_rooms}}>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="col" style="border: thin solid black">
                    <div class="justify-content-center align-items-center" style="display: flex; height: 100%;">
                      <strong style="font-size:1.3rem;" id="{{typeId}}-total-price">{{total_price}}</strong>&nbsp;{{currency}}
                    </div>
                  </div>
                </div>
              </form>
            </footer>
          </div>
        </div>
      </div>
    </article>
  </div>
  `
  return {
    /**
     * Displays slide by specified index.
     * @param {Object} payload => have description, price, capacity, available, images, title
     * @returns {jQuery}
     */
    init: function (payload) {
      // console.log("heissa.room-chooser => init function => params: payload ->", JSON.parse(JSON.stringify(payload)));
      let type = payload.name
      let typeId = payload.id
      let getInputIdSelector = (label) => (`[id='${typeId}-${label}']`)

      const config = Object.assign({type, typeId}, availableRooms, payload)
      var $component = $(Handlebars.compile(template)(config))

      var roomsInput = $component.find(getInputIdSelector('rooms'))
      var roomsDecreaseControl = $component.find(".room-control-decrease")
      var roomsIncreaseControl = $component.find(".room-control-increase")
      
      roomsIncreaseControl.on('valueChanged', payload.onValueChanged)
      roomsDecreaseControl.on('valueChanged', payload.onValueChanged)

      $component.getRoomValues = () => ({rooms: +roomsInput.val()})
      return $component
    }
  };
})(window.jQuery);
