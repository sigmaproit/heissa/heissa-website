---
---
var HeissaLogic = (function ($) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    });
    var Logic = {};
    Logic.params = {}

    const roomsStaticData = {{site.data.roomsTypes.rooms | jsonify}};
    const knownBenefits = {{site.data.benefits | jsonify}}
    // var roomsStaticData = HeissaHelpers.configToVar('_data/roomsTypes.json').rooms;

    var displayErrorTimeHandler = 0;
    Logic.helpers = {
        formatAvailabilityInput: (availabilityResponse) => {
            let rooms = availabilityResponse.rooms_info
            HeissaHelpers.dicForEach(rooms, (type, data) => {
                let roomStaticData = roomsStaticData.find(o => o.type === type)
                rooms[type].currency = availabilityResponse.currency
                rooms[type].benefits = rooms[type].benefits && rooms[type].benefits.length ? HeissaHelpers.leftJoin(rooms[type].benefits, knownBenefits, "name"): []
                rooms[type].no_of_beds = rooms[type].beds_count
                if (roomStaticData) {
                    rooms[type].images = roomStaticData.images
                    rooms[type].description = roomStaticData.desc
                } else {
                    rooms[type].images = ["{{site.data.roomsTypes.default_image}}"]
                    rooms[type].description = " "
                }
            })
            return rooms
        },

        readRoomsData: (ignoreEmpty=true) => {
            const result = {}
            if($("#roomsChooserContainer").children().length === 0) return result
            Array.from($("#roomsChooserContainer").children()).forEach(room => {
                const $room = $(room)
                const currTypeId = $room.attr("id")
                const currType = $room.attr("type")
                const roomsInput = $room.find(`#${currTypeId}-rooms`).val()
                if (ignoreEmpty && roomsInput === '0') return
                result[currType] = {
                    // type: $room.attr("type"),
                    id: currTypeId,
                    no_of_rooms: +roomsInput
                    // for further values
                }
            })
            return result
        },
        readCheckAvailabilityForm: () => {
            // take inputs into variables
            var numChildren = $("#noOfChildren").val(),
                numAdults = $("#noOfAdults").val(),
                departure = $("input[name=booking-form-departure-date]").val(),
                arrival = $("input[name=booking-form-arrival-date]").val(),
                bathroom = $("select[name=booking-form-bathroom]").val()

            // parse inputs
            arrival = moment(arrival, "MM/DD/YYYY").startOf('day').utc().format()
            departure = moment(departure, "MM/DD/YYYY").startOf('day').utc().format()
            numAdults = +numAdults
            numChildren = +numChildren

            return { arrival, departure, numChildren, numAdults, bathroom }
        },
        readCustomerReservationForm: () => ({
            name: $("#cf_name").val(),
            phone: $("#cf_phone").val(),
            email: $("#cf_email").val(),
        }),
        validateReservationData: data => {
            const customer = data.customer
            if (!(customer.name && customer.phone)) return { "status": false, "message": "name and phone can't be empty" }
            return { "status": true }
        },
        handleRequestError: (jqXHR, exception, config={}) => {
            var msg = config.eElse || 'unknown error happened!'
            if (jqXHR.status === 0) msg = config.e0 || 'Not connect.\n Verify Network.';
            else if (jqXHR.status >= 500) msg = config.e500 || 'Unexpected Server Error.';
            else if (jqXHR.status >= 400) msg = config.e400 || jqXHR.responseJSON.message || config.e400Else || "Unknown invalid arguments";
            else if (exception === 'parsererror') msg = config.eParse || 'Unexpected response, Try again later';
            else if (exception === 'timeout') msg = config.eTimeout || 'Time out error.';
            Logic.helpers.displayError(msg)
        },
        displayError: (msg, type = 'general', duration = 5000) => {
            const $ele = $(`#${type}-error-msg`)
            $ele.text(msg)
            $ele.parent("div").show("slow")
            clearTimeout(displayErrorTimeHandler)
            displayErrorTimeHandler  = setTimeout(() => $ele.parent("div").hide("slow"), duration)
        },

        updateUI: () => {
            if ($('[data-tooltip]').length && $.fn.MonkeysanTooltip) {
                $('[data-tooltip]').MonkeysanTooltip({
                    animationIn: 'fadeInDown',
                    animationOut: 'fadeOutUp',
                    tooltipPosition: 'top',
                    jQueryAnimationEasing: Milenia.ANIMATIONEASING,
                    jQueryAnimationDuration: Milenia.ANIMATIONDURATION,
                    skin: 'milenia'
                });
            }
            /* ----------------------------------------
                Dynamic background image
            ---------------------------------------- */
            var $backgrounds = $('[data-bg-image-src]:not([class*="milenia-colorizer--scheme-"])');
            if ($backgrounds.length && Milenia.helpers.dynamicBgImage) {
                Milenia.helpers.dynamicBgImage($backgrounds);
            }
            /* ----------------------------------------
                End of Dynamic background image
            ---------------------------------------- */

            /* ----------------------------------------
                Owl Carousel
            ---------------------------------------- */

            // owl carousel adaptive
            if ($('.owl-carousel').length) Milenia.helpers.owlAdaptive();

            var $simpleSlideshow = $('.milenia-simple-slideshow'),
                $testimonialsCarousel = $('.milenia-testimonials-inner.owl-carousel');

            if ($simpleSlideshow.length && $.fn.owlCarousel) {
                $simpleSlideshow.each(function (index, carousel) {
                    var $carousel = $(carousel),
                        $stretchedSection = $carousel.closest('.milenia-section--stretched-content, .milenia-section--stretched-content-no-px');

                    if ($stretchedSection.length) {
                        $stretchedSection.each(function (scindex, scelement) {
                            $(scelement).on('stretched.milenia.Section', function () {
                                $carousel.owlCarousel(Milenia.helpers.owlSettings({
                                    margin: 1,
                                    loop: true,
                                    autoplay: $carousel.hasClass('milenia-simple-slideshow--autoplay')
                                }));
                            });
                        });
                    }
                    else {

                        $carousel.owlCarousel(Milenia.helpers.owlSettings({
                            margin: 1,
                            loop: true,
                            autoplay: $carousel.hasClass('milenia-simple-slideshow--autoplay')
                        }));
                    }
                });
            }

            if ($testimonialsCarousel.length && $.fn.owlCarousel) {
                $testimonialsCarousel.each(function (index, carousel) {
                    var $carousel = $(carousel),
                        $stretchedSection = $carousel.closest('.milenia-section--stretched-content, .milenia-section--stretched-content-no-px');

                    if ($stretchedSection.length) {
                        $stretchedSection.on('stretched.milenia.Section', function () {
                            if ($carousel.data('owl.carousel')) return;
                            $carousel.owlCarousel(Milenia.helpers.owlSettings({
                                margin: 0,
                                loop: true,
                                nav: false,
                                dots: true
                            }));
                        });
                    }
                    else {
                        $carousel.owlCarousel(Milenia.helpers.owlSettings({
                            margin: 0,
                            loop: true,
                            nav: false,
                            dots: true
                        }));
                    }
                });
            }

            Milenia.helpers.gridOwl.extendConfigFor('.milenia-testimonials', {
                nav: false,
                dots: true,
                startPosition: 1,
                autoplay: true,
                loop: true
            });

            Milenia.helpers.gridOwl.extendConfigFor('.milenia-tabbed-carousel-thumbs', {
                nav: true,
                dots: false,
                margin: 0,
                loop: false, // !important
                autoplay: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    },
                    1300: {
                        items: 4
                    }
                },
                responsiveWithSidebar: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    1350: {
                        items: 3
                    }
                }
            });

            // Initialization owl carousels placed in the stretched sections
            $('[class*="milenia-section--stretched-content"]').on('stretched.milenia.Section', function (event, $section) {
                var $gridOwlCarousels = $section.find('.milenia-grid.owl-carousel'),
                    $simpleThumbs = $section.find('.milenia-simple-slideshow-thumbs.owl-carousel');

                if ($gridOwlCarousels.length) Milenia.helpers.gridOwl.add($gridOwlCarousels);

                if ($simpleThumbs.length) {
                    $simpleThumbs.owlCarousel(Milenia.helpers.owlSettings({
                        responsive: {
                            0: {
                                items: 2
                            },
                            380: {
                                items: 3
                            },
                            992: {
                                items: 4
                            },
                            1200: {
                                items: 6
                            }
                        },
                        margin: 10,
                        loop: false
                    }));
                }
            });

            // Initialization owl carousels placed in the normal sections
            var $simpleThumbs = $('.milenia-simple-slideshow-thumbs.owl-carousel').filter(function (index, element) {
                return !$(element).closest('[class*="milenia-section--stretched-content"]').length;
            });

            if ($simpleThumbs.length) {
                $simpleThumbs.owlCarousel(Milenia.helpers.owlSettings({
                    responsive: {
                        0: {
                            items: 2
                        },
                        380: {
                            items: 3
                        },
                        992: {
                            items: 4
                        },
                        1200: {
                            items: 6
                        }
                    },
                    margin: 10,
                    loop: false
                }));
            }

            Milenia.helpers.gridOwl.add($('.milenia-grid.owl-carousel:not(.owl-loaded)').filter(function (index, element) {
                return !$(element).closest('[class*="milenia-section--stretched-content"]').length;
            }));

            Milenia.helpers.owlSyncTabbed.init($("[data-tabbed-sync]"));
            Milenia.helpers.owlSync.init();
            /* ----------------------------------------
                 End of Owl Carousel
             ---------------------------------------- */

            /* ----------------------------------------
                 Tabbed Grid
             ---------------------------------------- */

            if (window.MileniaTabbedGrid) {
                window.MileniaTabbedGrid.init($('.milenia-grid--tabbed'), {
                    cssPrefix: 'milenia-',
                    easing: Milenia.ANIMATIONEASING,
                    duration: Milenia.ANIMATIONDURATION
                });

                $('[class*="milenia-section--stretched-content"]').on('stretched.milenia.Section', function (event, $section) {
                    var $gridTabbed = $section.find('.milenia-grid--tabbed');

                    setTimeout(function () {
                        if ($gridTabbed.length) $gridTabbed.data('TabbedGrid').resize();
                    }, Milenia.ANIMATIONDURATION);
                });

                $('.milenia-grid--tabbed').on('grid.resized.tabbedgrid item.shown.tabbedgrid', function (event, $grid) {
                    if ($grid.data('TabsResizeTimeOutId')) clearTimeout($grid.data('TabsResizeTimeOutId'));
                    Milenia.helpers.updateGlobalNiceScroll();

                    $grid.data('TabsResizeTimeOutId', setTimeout(function () {
                        var $tabs = $grid.closest('.milenia-tabs'),
                            Tabs;
                        if (!$tabs.length) return;

                        Tabs = $tabs.data('tabs');

                        if (Tabs) Tabs.updateContainer();
                    }, 100));
                });
            }

            /* ----------------------------------------
                 End of Tabbed Grid
             ---------------------------------------- */
        }
    }
    

    const setLoading = (selector, loading=true) => $(selector).attr('loading', loading)
    Logic.afterDOMReady = async function () {
        // on check availability clicked
        $('#book_btn_checkAvailability').click( ({target}) => $(target).attr("loading") === "true" ? null : Logic.checkAvailability());

        // to reset all suggestions after change the form data
        $(".reset-suggestions").on("change", (e) => {
            $('#roomsChooserContainer').empty()

            $(".after-checking-Availability").addClass("before-checking-Availability")
            $(".after-checking-Availability").removeClass("after-checking-Availability")
        })

        // on home check availability button clicked
        $('#home-checkAvailability-btn').click(function (e) {
            e.preventDefault()
            window.location.replace(`/book?${$(".milenia-booking-form").serialize()}`)
        });

        // on reservation button clicked
        $('#btn-reservation-submit').click(function (e) {
            // it is handled from the contact form directly
            // e.preventDefault()

            // Logic.sendReservation()
        });

        // add date delay
        $(".milenia-field-datepicker[data-delay]").each((index, ele) => {
            const $ele = $(ele)
            let now = moment() // already in Cairo tz
            let shifted = now.add($ele.data('delay'), 'days')
            let $input = $ele.siblings('input')
            if($input.length === 0) $input = $ele.children('input')
            //? the jquery ui datepicker doesn't work when you attached to input, you should change from the input... else we will facing datepicker showing at the end issue
            //? so never use here the $input.datepicker("setDate", shifted.format("MM/DD/YYYY"))
            //? another thing, if the last input (departure date) has an id, this is somehow affects the datepicker and make it show, so again make sure to remove the inputs id's of dates
            $input.val(shifted.format("MM/DD/YYYY"))
            $input.trigger("change")
            // $input.trigger("change", [shifted]) // force changing the span
        })

        if(window.HeissaRoomsClassifications){
            $.ajaxWithRecaptchaHeader("{{site.data.api.host}}{{site.data.api.root}}{{site.data.api.listTypes}}", {
                dataType: "json",
                success: priceData => {
                    // TODO: handle pages logic
                    const items = priceData.items
                    $(".milenia-tabbed-carousel[data-rooms-classification]").each((index, ele) => {
                        const $ele = $(ele)
                        const knownStaticRooms = $ele.data('rooms-classification')
                        const showStartingPrince = $ele.data('show-price')
                        // const knownBenefits = {}
                        // const rooms = items.filter(item => knownStaticRooms.find(room => item.name === room.type) != undefined).map(item => {
                        const rooms = items.map(item => {
                            const targetRoom = knownStaticRooms.find(room => item.name === room.type)
                            const images = targetRoom ? targetRoom.images : ["{{site.data.roomsTypes.default_image}}"]
                            const desc = targetRoom ? targetRoom.desc : " "
                            item.beds_count = item.beds_count || "?"
                            item.show_starting_price = showStartingPrince
                            const benefits = item.benefits && item.benefits.length ? HeissaHelpers.leftJoin(item.benefits, knownBenefits, "name"): []
                            return { ...item, images, desc, benefits }
                        })
                        // console.log("rooms are: ", rooms);
                        const $domchild = HeissaRoomsClassifications.init({ rooms })
                        $domchild.appendTo(ele)
                        Logic.helpers.updateUI()
                    })
                },
                error: (jqXHR, exception) => {
                    console.log("Can't get the types");
                }
            });

        }
    }

    Logic.checkAvailability = () => {
        const reqData = Logic.helpers.readCheckAvailabilityForm()
        var reqParams = {
            start_date: reqData.arrival,
            end_date: reqData.departure,
            no_of_adults: reqData.numAdults,
            no_of_children: reqData.numChildren,
            benefits: {}
        }
        // if(reqData.bathroom === "no"){
        //     // invalid
        //     Logic.helpers.displayError("select bathroom")
        //     return
        // }
        // if(reqData.bathroom !== "any") {
        //     reqParams.benefits.private_bathroom = reqData.bathroom === "private"
        // }
        
        setLoading('#book_btn_checkAvailability', true)
        $.ajaxWithRecaptchaHeader({
            'url': "{{site.data.api.host}}{{site.data.api.root}}{{site.data.api.checkAvailability}}",
            'method': "POST",
            'data': JSON.stringify(reqParams),
            'dataType': "json",
            'success': (suggestionsData) => {
                Logic.onCheckAvailabilityResponse(reqData, suggestionsData)
            },
            'error': function (jqXHR, exception) {
                Logic.helpers.handleRequestError(jqXHR, exception)
            },
            'complete': () => {
                setLoading('#book_btn_checkAvailability', false)
            }
        });
    }

    Logic.onCheckAvailabilityResponse = (reqData, resData) => {
        // re-enable the button after any response
        setLoading('#book_btn_checkAvailability', false)

        // check empty response
        if (!resData || resData.error) {
            // no data returned
            alert("No rooms available for your request")
            return
        }

        const { numChildren, numAdults } = reqData

        // progressbar initializing
        if (window.HeissaProgressbar) {
            var $collection = $('.heissa-progressbar')
            HeissaProgressbar.init($collection, { max: +numAdults + +numChildren })
        }

        // empty the booking form
        $(".milenia-contact-form").trigger("reset")

        // rooms filling
        $('.before-checking-Availability').addClass("after-checking-Availability")
        $('.before-checking-Availability').removeClass('before-checking-Availability')
        var payload = Logic.helpers.formatAvailabilityInput(resData)

        payload.onChange = HeissaHelpers.debounce(Logic.calculatePrice, 1000)
        var $domElements = HeissaRoomsChooser.init(payload)

        $('#roomsChooserContainer').empty()
        $domElements.appendTo('#roomsChooserContainer')

        // TODO: remove the code duplication of this function in the logic and milenia files
        Logic.helpers.updateUI()

        // initial update
        Logic.onPriceUpdated({ no_of_adults: numAdults, no_of_children: numChildren }, resData)
    }

    Logic.calculatePrice = () => {
        // get req values
        const availabilityFormData = Logic.helpers.readCheckAvailabilityForm()
        const reqValues = {
            start_date: availabilityFormData.arrival,
            end_date: availabilityFormData.departure,
            no_of_adults: availabilityFormData.numAdults,
            no_of_children: availabilityFormData.numChildren,
            room_types: Logic.helpers.readRoomsData()
        }
        
        // validate the data, so that see should make the request or not


        // abort old request
        if (Logic.priceRequest) Logic.priceRequest.abort()

        // disable clicking on submit till the request returned, so that customer maker sure that reservation is done in last recent change
        $("#btn-reservation-submit").prop('disabled', true)

        // initiate the request and save the variable as we may abort
        Logic.params.priceRequest = $.ajaxWithRecaptchaHeader("{{site.data.api.host}}{{site.data.api.root}}{{site.data.api.price}}", {
            method: "post",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(reqValues),
            success: priceData => Logic.onPriceUpdated(reqValues, priceData),
            error: (jqXHR, exception) => {
                Logic.helpers.handleRequestError(jqXHR, exception)
                Logic.setInvalidPriceState()
            }
        });
    }

    Logic.onPriceUpdated = (reqData, resData) => {
        // enable the reservation button
        $("#btn-reservation-submit").prop('disabled', false)

        // helpers
        const newPriceRow = (title, cost, currency) => `<div class="row heissa-recite-row"><div class="col-8">${title}</div><div class="col-4" style="text-align: end">${cost} ${currency}</div></div>`

        // all required inputs
        let { no_of_adults, no_of_children } = reqData
        let { start_date, end_date, number_of_days, rooms_info, applied_children_discount, final_price, currency } = resData

        // reformat inputs
        start_date = new Date(start_date)
        end_date = new Date(end_date)
        const getOrdinalSuffix = d => d === 1 ? "st" : d === 2 ? "nd" : d === 3 ? "rd" : "th"
        const getFullMonthName = m => ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][m];
        const getOrdinalDate = date => `${date.getDate()}${getOrdinalSuffix(date.getDate())} ${getFullMonthName(date.getMonth())}`
        start_date = getOrdinalDate(start_date)
        end_date = getOrdinalDate(end_date)

        number_of_days = '(' + number_of_days + (number_of_days == 1 ? " night" : " nights") + ')'
        no_of_adults = '' + no_of_adults + 'x adult'
        no_of_children = '' + no_of_children + 'x child'

        // write direct result
        $("#recite-book-start-date").text(start_date)
        $("#recite-book-end-date").text(end_date)
        $("#recite-book-duration").text(number_of_days)
        $("#recite-adults").text(no_of_adults)
        $("#recite-children").text(no_of_children)
        $("#recite-total-price").text(final_price).css("color", "black")

        // write each room type price
        HeissaHelpers.dicForEach(rooms_info, (type, values) => {
            if (values instanceof Function) return
            $(`#${values.id}-total-price`).text(values.total_price).css("color", "black")
        })
        HeissaHelpers.dicForEach(Logic.helpers.readRoomsData(false), (type, values) => {
            values.no_of_rooms === 0 && 
            $(`#${values.id}-total-price`).text("0").css("color", "black")
        })
        
        
        // write dynamic recite prices
        let $recitePricesContainer = $("#recite-prices-container")
        $recitePricesContainer.empty()
        HeissaHelpers.dicForEach(rooms_info, (type, values) => {
            if (values.filled_rooms === 0 || values instanceof Function) return
            const elementStr = newPriceRow(`${values.filled_rooms || values.rooms.length}x ${type}`, values.total_price, currency)
            $recitePricesContainer = $recitePricesContainer.append(elementStr)
        })
    }

    Logic.setInvalidPriceState = () => {
        $("#recite-total-price").text("X").css("color", "red")
        $("#recite-prices-container").empty()
        HeissaHelpers.dicForEach(Logic.helpers.readRoomsData(false), (type, values) => {
            if (values instanceof Function) return
            const $ele = $(`#${values.id}-total-price`)
            if (values.no_of_rooms === 0) $ele.text("0").css("color", "black")
            else $ele.text("X").css("color", "red")
        })
    }


    Logic.sendReservation = () => {
        if (Logic.params.reservationRequest) return // wait the request
        const {arrival, departure, numAdults, numChildren} = Logic.helpers.readCheckAvailabilityForm()
        const room_types = Logic.helpers.readRoomsData()
        const customer = Logic.helpers.readCustomerReservationForm()
        const reqData = {
            start_date: arrival,
            end_date: departure,
            no_of_adults: numAdults,
            no_of_children: numChildren,
            room_types,
            customer,
            customer_note: $('#cf_notes').val(),
        }
        const validation = Logic.helpers.validateReservationData(reqData)
        if(!validation.status) return Logic.helpers.displayError(validation.message)
        Logic.params.reservationRequest = true
        setLoading("#btn-reservation-submit")
        $.ajaxWithRecaptchaHeader("{{site.data.api.host}}{{site.data.api.root}}{{site.data.api.createReservation}}", {
            method: "post",
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success: function (res) {
                window.location.replace(`/thanks.html`)
            },
            error: function (jqXHR, exception) {
                Logic.helpers.handleRequestError(jqXHR, exception)
            },
            complete: () => {
                Logic.params.reservationRequest = false
                setLoading("#btn-reservation-submit", false)
            }
        })
    }

    Logic.afterOuterResourcesLoaded = () => {
        $.ajaxWithRecaptchaHeader("{{site.data.api.host}}{{site.data.api.root}}{{site.data.api.weather}}", {
            dataType: "json",
            success: response => {
                const weatherData = {
                    temp: Math.round(+response.main.temp - 273),
                    text: response.weather[0].description
                }
                $("#footer-current-weather-value").text(weatherData.temp)
            }
        })
    }

    return Logic
})(jQuery);
