

function fill() {
    var params = new URLSearchParams(window.location.search);
    var names = ["booking-form-arrival-date", "booking-form-departure-date", "booking-form-adults", "booking-form-children", "booking-form-bathroom"]

    var bookingForm = $(".milenia-booking-form--redirect")
    if (!bookingForm.length) return
    var changed = false
    names.forEach((name) => {
        var value = params.get(name)
        if (!value) return
        var input = bookingForm.find(`input[name=${name}]`)
        if (name == "booking-form-bathroom") {
            var ol = $(".milenia-custom-select").data("customSelect").optionsList
            ol.children().each((_, li) => { (li.dataset.value == value) && li.click() })
        }
        if (!input.length) return
        var $value = input.siblings('.milenia-field-counter-value')
        if ($value.length) {
            $value.text(value)
        }
        input.val(value)
        input.trigger("change") // to update the date spans: this line causes a call to the function registered at fieldDatepicker in milenia.app.js file at line 1489
        changed = true
    })
    if (changed) {
        $(".milenia-booking-form--redirect").find("#book_btn_checkAvailability").click()
    }
}