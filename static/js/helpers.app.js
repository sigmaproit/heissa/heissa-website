
/// <reference path="../../node_modules/@types/jquery/index.d.ts" />
var HeissaHelpers = (function () {
    var helpers = {}
    
    helpers.configToVar = (path) => {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': path,
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    };

    helpers.dicToArr = (dict) => {
        var res = []
        for (var key in dict) {
            // check if the property/key is defined in the object itself, not in parent
            if (dict.hasOwnProperty(key) && !(dict[key] instanceof Function)) {           
                res.push(dict[key])
            }
        }
        return res
    }

    helpers.dicForEach = (dict, callback) => {
        for (var key in dict) {
            // check if the property/key is defined in the object itself, not in parent
            if (dict.hasOwnProperty(key)) {           
                callback(key, dict[key])
            }
        }
    }

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    helpers.debounce = (func, wait, immediate) => {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    helpers.leftJoin = (arr1, arr2, key1, key2) => {
        key2 = key2 || key1
        const res = []
        arr1.forEach(i1 => {
            const targetI2 = arr2.find(i2 => i1[key1] === i2[key2])
            if(!targetI2) res.push(i1)
            else res.push({...i1, ...targetI2})
        })
        return res
    }

    helpers.getCurrentWeather = async (url) => {
        // var countryId = "{{site.wheather.countryId}}"
        // var appid = "{{site.wheather.apiKey}}"
        var response = await $.get(url)
        // response = response.json
        var res = {
            temp: +response.main.temp - 273,
            text: response.weather[0].description
        }
        return res
    }
    return helpers;
})() 