---
---
$.ajaxWithRecaptchaHeader = function () {
    var args = [...arguments]
    if(args.length === 0) return
    return grecaptcha.ready(function () {
        grecaptcha.execute('{{site.reCAPTCHA.siteKey}}', { action: 'submit' }).then(function (token) {
            var settingsIndex = args.length >= 2 ? 1 : 0
            if (!args[settingsIndex].headers) args[settingsIndex].headers = {}
            args[settingsIndex].headers["X-RECAPTCHA"] = token
            return $.ajax(...args)
        });
    });
}
